import logo from './logo.svg';
import './App.css';

function App() {

var response ;

fetch('https://jsonplaceholder.typicode.com/posts')
    .then(res => res.json())
    .then(data => response = data)
    .then(() => {
//       console.log(response)
      displayData(response);
      
    });


function displayData(response){
//   console.log(response);
  response.map(item => {
  
  const displayDiv = document.getElementById('display');
  var detailContainer = document.createElement('div');
  var title =document.createElement('button');
  title.innerHTML= item.title;
  title.id= item.id;
  title.classList.add('modal','show');
  title.classList.add('button');
  detailContainer.appendChild(title);
  
//   console.log(title.innerHTML);
//   console.log(title.id);
  var titleDetail = document.createElement('div');
  titleDetail.innerHTML = item.title;
  titleDetail.classList.add('modal');
    
  detailContainer.appendChild(titleDetail);
  
  var description = document.createElement('div');
  description.innerHTML = item.body;
  description.id= item.id+100;
  description.classList.add('modal');
  detailContainer.appendChild(description);
    
  var buttonClose = document.createElement('button');
   buttonClose.classList.add('modal');
    buttonClose.classList.add('button');
   buttonClose.innerHTML='close';
    
   buttonClose.addEventListener('click', ()=>{
     const description=document.getElementById(item.id+100);
     description.classList.remove('show');
     title.classList.remove('hide');
     buttonClose.classList.remove('show');
     titleDetail.classList.remove('show');
   })
   
   detailContainer.appendChild(buttonClose);
//   console.log(description.innerHTML);
//     console.log(detailContainer);
  
  title.addEventListener('click', ()=> {
      const description=document.getElementById(item.id+100);
      description.classList.add('show');
      const title=document.getElementById(item.id);
      title.classList.add('hide');
      buttonClose.classList.add('show');
      titleDetail.classList.add('show');
    
  } )
  displayDiv.appendChild(detailContainer);
  
  })
  
  
  }

  return (
    <div id="display">
    
    </div>
  );
}

export default App;
